﻿(function () {
    'use strict';

    angular
        .module('SpatialVisionTask')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['UserService', '$rootScope', '$scope','MapService', 'FlashService'];
    function HomeController(UserService, $rootScope, $scope, MapService, FlashService) {
       
	   var vm = this;
		
       $scope.user = null;
       $scope.allUsers = [];
		$scope.editMode = true;
		$scope.dataLoading = false;
        
        $scope.initController = function() {
			MapService.init();
            $scope.loadCurrentUser();
            $scope.loadAllUsers();
        }
		
        $scope.loadCurrentUser = function() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                     $scope.user = user;
					 $scope.user.dob=moment($scope.user.dobString); 
					 MapService.addMarker($scope.user.cityDetails);
                });
        }

        $scope.loadAllUsers = function() {
            UserService.GetAll()
                .then(function (users) {
                    $scope.allUsers = users;
                });
        }

		$scope.$watch(function ($scope) { return $scope.chosenPlaceDetails }, function () {
            if (angular.isDefined($scope.chosenPlaceDetails )) {
               console.log($scope.chosenPlaceDetails );
			    MapService.addMarker($scope.chosenPlaceDetails);
            }
        });
		$scope.activateEdit = function()
		{
			$scope.editMode = false;
			console.log($scope.editMode);
		}
		
		$scope.validate = function()
		{
		if($scope.user.dob === null || $scope.user.dob === "" || $scope.user.dob === undefined)
		{
			$scope.dobrequired = true;
			return false;
		}
		else if($scope.user.city === null || $scope.user.city === "" || $scope.user.city === undefined)
			{
				$scope.cityrequired = true;
				return false;
			}
			
		else
		{
			$scope.dobrequired = false;
			$scope.cityrequired = false;
			return true;
		}
		}
		$scope.Update = function()
		{
			if($scope.validate())
			{
			$scope.dataLoading = true;
			UserService.Update($scope.user).
			then(function () {
                        FlashService.Success('Update successful', true);
                        $scope.dataLoading = false;
						$scope.editMode = true;
                    
                })
				.catch(function (error) { 
					console.error(error); // Note: this logs the js type error
				});
			}
		}
		$scope.initController();
		
    }

})();