(function () {
    'use strict';

angular.module('SpatialVisionTask')
    .service('MapService', function($q) {
    
    this.init = function() {
        var options = {
            center: new google.maps.LatLng(40.7127837, -74.00594130000002),
            zoom: 13,
            disableDefaultUI: true    
        }
		console.log("hello");
        this.map = new google.maps.Map(
            document.getElementById("map"), options
        );
		console.log
		console.log("this.map"+this.map);
        this.places = new google.maps.places.PlacesService(this.map);
    }
	
	this.addMarker = function(res) {
        if(this.marker) this.marker.setMap(null);
        this.marker = new google.maps.Marker({
            map: this.map,
            position: res.geometry.location,
            animation: google.maps.Animation.DROP
        });
        this.map.setCenter(res.geometry.location);
    }
    
	})
angular.module('SpatialVisionTask')
    .directive('googleplace', function() {
    return {
        require: 'ngModel',
        scope: {
            ngModel: '=',
            details: '=?'
        },
        link: function(scope, element, attrs, model) {
            var options = {
                types: []
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                scope.$apply(function() {
                    scope.details = scope.gPlace.getPlace();
					console.log(scope.details);
					
                    model.$setViewValue(element.val());                
                });
            });
        }
    };

})
	
	})();