﻿(function () {
    'use strict';

    angular
        .module('SpatialVisionTask')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location','$scope','AuthenticationService', 'FlashService','$rootScope'];
    function LoginController($location, $scope, AuthenticationService, FlashService, $rootScope) {


        (function initController() {
            // reset login status
			$rootScope.loggedin=false;
            AuthenticationService.ClearCredentials();
        })();

        $scope.login = function() {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
					$rootScope.loggedin= true;
                    $location.path('/');
                } else {
                    FlashService.Error(response.message);
                    $scope.dataLoading = false;
                }
            });
        };
    }

})();
