﻿(function () {
    'use strict';

    angular
        .module('SpatialVisionTask')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService','$scope','$location', '$rootScope', 'FlashService', 'MapService'];
    function RegisterController(UserService,$scope, $location, $rootScope, FlashService, MapService) {
		$scope.dataLoading = false;
		$scope.MapService = MapService;
		
		$scope.validate = function()
		{
		if($scope.user.dob === null || $scope.user.dob === "" || $scope.user.dob === undefined)
		{
			$scope.dobrequired = true;
			return false;
		}
		else if($scope.user.city === null || $scope.user.city === "" || $scope.user.city === undefined)
			{
				$scope.cityrequired = true;
				return false;
			}
			
		else
		{
			$scope.dobrequired = false;
			$scope.cityrequired = false;
			return true;
		}
		}
        $scope.register = function() {
			if($scope.validate())
			{
            $scope.dataLoading = true;
            UserService.Create($scope.user)
                .then(function (response) {
                    if (response.success) {
                        FlashService.Success('Registration successful', true);
                        $location.path('/login');
                    } else {
                        FlashService.Error(response.message);
                        $scope.dataLoading = false;
                    }
                });
			}
        }
		
		$scope.$watch(function ($scope) { return $scope.chosenPlaceDetails }, function () {
            if (angular.isDefined($scope.chosenPlaceDetails )) {
               console.log($scope.chosenPlaceDetails );
			    MapService.addMarker($scope.chosenPlaceDetails);
				$scope.user.cityDetails = $scope.chosenPlaceDetails;
            }
        });
		MapService.init();
		
    }

})();
